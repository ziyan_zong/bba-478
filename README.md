Story
We try to identify if customers who are loyal customer with a preferred payment option of Klarna have a higher net spend in a season than non Klarna.

Approach
Filter: DE, DIRECT, AW 16/17, Customer Group: [AC-1, AC-2]
Get all email adresses from sandbox
Classify each customer as Klarna / NonKlarna / Mixed customer (Mixed Customer: Klarna Share of Orders > 0% and <= 50%)
Show distribution and Mean of Net Spend per Season per Customer Group
Establish if there's a difference between groups in terms of Net Spend
Rory bets Klarna is less for a beer

